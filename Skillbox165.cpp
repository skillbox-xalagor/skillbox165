// Skillbox165.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>

int main()
{
    //�������� ������� ���� � ������
    time_t now = time(0);
    tm *ltm = localtime(&now);
    std::cout << "Current day is " << ltm->tm_mday << '\n';

    //������ ��������� ������ ����������� NxN � ��������� ��� ���, ����� ������� � ��������� i � j ��� ����� i + j
    std::cout << "Array:\n";
    const int N = 10;
    int Arr[N][N];
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            Arr[i][j] = i + j;
            std::cout << Arr[i][j] << '\t';
        }
        std::cout << '\n';
    }

    //������� ����� ��������� � ������ �������, ������ ������� ����� ������� ������� �������� ����� ��������� �� N
    int LineNumber = ltm->tm_mday % N;
    int Sum = 0;
    for (int i = 0; i < N; i++) {
        Sum += Arr[LineNumber][i];
    }
    std::cout << "Number sum in line (" << ltm->tm_mday << "%" << N << ") is " << Sum << '\n';
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
